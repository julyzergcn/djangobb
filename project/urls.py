from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView
from django.conf import settings
from djangobb_forum import settings as forum_settings
from project.sitemap import SitemapForum, SitemapTopic

admin.autodiscover()

sitemaps = {
    'forum': SitemapForum,
    'topic': SitemapTopic,
}

urlpatterns = patterns('',
    # auto redirect to /form/
    url(r'^$', RedirectView.as_view(url='/forum/')),
    
    url(r'^forum/account/', include('django_authopenid.urls')),
    url(r'^forum/', include('djangobb_forum.urls', namespace='djangobb')),
    
    # Sitemap
    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    
    url(r'^admin/', include(admin.site.urls)),
)

# PM Extension
if forum_settings.PM_SUPPORT:
    urlpatterns += patterns('',
        (r'^forum/pm/', include('django_messages.urls')),
   )

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^%s(?P<path>.*)$' % settings.MEDIA_URL.lstrip('/'),
            'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )